/**
 * @file CKEditor Entity Embed - Plugin for embedding Drupal entities
 */
( function() {
  // Uncommenting and changing this timestamp value forces the widget to be
  // reloaded on the next page load. This is very handy while developing.
  // CKEDITOR.timestamp = 'changemeifneeded';

  CKEDITOR.plugins.add( 'entityembed',
  {
    requires : [ 'dialog' ],
    init: function( editor )
    {
      editor.addCommand( 'entityembedDialog', new CKEDITOR.dialogCommand( 'entityembedDialog' ) );
      editor.ui.addButton( 'EntityEmbed',
      {
        label: Drupal.settings.ckeditor_entity_embed.label_plugin,
        command: 'entityembedDialog',
        icon: this.path + 'images/icon.png'
      } );
      CKEDITOR.dialog.add( 'entityembedDialog', this.path + 'dialogs/entityembed.js' );
    }
  } );
} )();
