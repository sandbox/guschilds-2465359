/**
 * @file
 * Dialog window definition for the entityembed plugin.
 *
 * http://docs.ckeditor.com/#!/api/CKEDITOR.dialog
 */

CKEDITOR.dialog.add( 'entityembedDialog', function( editor ) {
  return {
    title : Drupal.settings.ckeditor_entity_embed.label_plugin,
    minWidth : 400,
    minHeight : 120,
    contents : [
    {
      id : 'entityTab',
      label : Drupal.t('Embed entity selection'),
      title : Drupal.t('Embed entity selection'),
      elements :
      [
      {
        id : 'drupal_entity',
        type : 'text',
        label : Drupal.settings.ckeditor_entity_embed.label_search,
        onLoad: function() {
          // Add autocomplete behavior to this (inspired by ckeditor_link).
          var element = this.getInputElement();
          var acPath = Drupal.settings.ckeditor_entity_embed.autocomplete_path;
          element.addClass('form-autocomplete');
          element.$.setAttribute('autocomplete', 'OFF');
          new Drupal.jsAC(jQuery(element.$), new Drupal.ACDB(acPath));
        }
      },
      {
        id : 'alignment',
        type : 'select',
        label : Drupal.t('Alignment'),
        items: [ [ 'Left' ], [ 'Center' ], [ 'Right' ], [ 'Full-width' ] ],
        'default' : 'Left'
      }
      ]
    }
    ],
    onOk : function() {
      // Grab the token from the selected item and insert it into the editor.
      // Ex: '[embed:render:node:1:left]'.
      var editor = this.getParentEditor();
      var entity = this.getValueOf( 'entityTab', 'drupal_entity' );
      var alignment = this.getValueOf( 'entityTab', 'alignment' );
      var matches = entity.match(/\[(.*?)\]/);
      if ( matches.length>0 ) {
        var tokenType = Drupal.settings.ckeditor_entity_embed.token_type;
        var token = '[' + tokenType + ':render:' + matches[1] + ':' + alignment.toLowerCase() + ']';
        editor.insertText(token);
      }
    }
  };
});
