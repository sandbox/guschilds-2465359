<?php

/**
 * @file
 * Default theme implementation to display a rendered embedded entity.
 *
 * Available variables:
 * - $entity: An array of entity items to be rendered and embedded.
 * - $entity_type: The type of the entity to be rendered and embedded.
 * - $entity_id: The id of the entity to be rendered and embedded.
 * - $view_mode: The view mode that the entity has been rendered through.
 * - $alignment: The chosen alignment of the rendered entity. A class based on
 *   this choice is added to the wrapper element and can be used to properly
 *   align the embedded entity.
 * - $wrapper_attributes: A string of attributes to assign to the wrapping
 *   element. The classes of $classes_array have been flattened into it.
 * - $classes_array: An array of classes to assign to the wrapping element.
 *
 * @see template_preprocess_ckeditor_entity_embed()
 *
 * @ingroup themeable
 */
?>
<?php if ($entity): ?>
  <div<?php print $wrapper_attributes; ?>>
    <?php print render($entity); ?>
  </div>
<?php endif; ?>
