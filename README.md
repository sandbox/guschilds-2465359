# CKEditor Entity Embed

Embed entities in text fields that use CKEditor.

Contrib dependencies:

* [CKEditor](https://www.drupal.org/project/ckeditor)
* [Entity API](https://www.drupal.org/project/entity)
* [Token Filter](https://www.drupal.org/project/token_filter)

Similar (but different) modules:

* [Media](https://www.drupal.org/project/media)
* [Node Embed](https://www.drupal.org/project/node_embed)
* [Token Insert Entity](https://www.drupal.org/project/token_insert_entity)

## Installation

1. Enable the module.
2. Navigate to admin/config/content/ckeditor/ and click "edit" next to the text
format this functionality should be added to.
3. Expand "Editor Appearance" on the resulting page, find the EntityEmbed icon
in the "Available buttons" and drag it into the "Current toolbar". Also, in the
"Plugins" section, enable the plugin for this module. Save the form.
4. Navigate to admin/config/content/formats/ and click "configure" next to the
same text format used in step 3.
5. Under "Enabled filters", enable "Replace tokens". If the "Limit allowed HTML
tags" filter is in use (such as with the "Filtered HTML" format), you may want
to move "Replace tokens" below it to avoid rendered entities from having HTML
removed (keep security in mind when doing so). Save the form.
6. Navigate to admin/config/content/ckeditor_entity_embed/ and enable the types
of entities that should appear in the embeddable results. Labeling used within
the interface can also be configured here. Save the form.

## Use

1. After following the Installation instructions, navigate to an entity form
that has a field using CKEditor and the text format you've added the button and
filter too.
2. Click the "Embed content" icon in the CKEditor tool bar.
3. Search for the content to be embedded and select an entity from the
autocomplete results. If the expected content is not appearing, ensure
Installation step 7 was properly followed.
4. Select alignment for that entity.*
5. Click OK. A token should appear among the CKEditor value. That token will be
replaced by the rendered entity wherever this field is rendered and viewed.

*Note: The alignment will have no effect until styles are added to your theme.
The rendered entity will be wrapped in an element with classes like
```ckeditor-entity-embed``` and ```ckeditor-entity-embed--left``` (these classes
can be altered in a preprocess function). Use the second class in that pair to
restrict widths, float the element accordingly, etc.

## Customization

This module adds a view mode to all entities that can be embedded. Navigate to
the "Manage display" interface for those entities to configure what fields are
displayed in the "CKEditor Embed" view mode and what formatters those fields
should use.

This module also provides two theme suggestions for the view mode used. These
hooks depend on the entity type, but for nodes they look like this:

* ```node__ckeditor_embed```
* ```node__article__ckeditor_embed```

This allows templates like node--article--ckeditor-embed.tpl.php to fully
control the markup displayed by nodes of a particular content type being
rendered with this functionality.

Furthermore, the wrapper element around the rendered entity can be altered using
```hook_preprocess_ckeditor_entity_embed()``` and/or by overriding the
ckeditor-entity-embed.tpl.php template file in the templates directory.

Always use the theme suggestions to alter the entity itself and the
```ckeditor_entity_embed``` theme implementation to alter the wrapper.

See admin/config/content/ckeditor_entity_embed/ for configuration options.
