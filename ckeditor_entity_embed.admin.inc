<?php

/**
 * @file
 * Administrative pages for the CKEditor Node Embed module.
 */

/**
 * Configuration callback for module settings.
 */
function ckeditor_entity_embed_admin($form, &$form_state) {
  $form = array();

  $form['entity-bundles'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Embeddable entity/content types'),
    '#description' => t('Viewable types selected here will be available to embed
      within the CKEditor Entity Embed plugin.'),
    '#default_value' => ckeditor_entity_embed_admin_entity_bundle_default_values(),
    '#options' => ckeditor_entity_embed_admin_entity_bundle_options(),
  );

  // Labels within the plugin interface.
  $form['plugin'] = array(
    '#type' => 'fieldset',
    '#title' => t('CKEditor plugin interface'),
    '#collapsible' => TRUE,
  );
  $form['plugin']['label-plugin'] = array(
    '#type' => 'textfield',
    '#title' => t('Plugin label'),
    '#description' => t('This label will appear when a user hovers over the icon
      that represents this plugin in the CKEditor toolbar. It will also be the
      title of the dialog that appears when that icon is clicked.'),
    '#default_value' => ckeditor_entity_embed_plugin_label('plugin'),
  );
  $form['plugin']['label-search'] = array(
    '#type' => 'textfield',
    '#title' => t('Search field label'),
    '#description' => t('The label for the search field within the dialog.'),
    '#default_value' => ckeditor_entity_embed_plugin_label('search'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#submit' => array('ckeditor_entity_embed_admin_submit'),
  );

  return $form;
}

/**
 * Submit handler for the configuration.
 */
function ckeditor_entity_embed_admin_submit($form, &$form_state) {
  // Reformat the selected options into a multi-dimensional array whose keys are
  // selected entity types and values are arrays of selected bundles. This makes
  // it easier/faster to use these values when needed.
  $selections = array();
  foreach ($form_state['values']['entity-bundles'] as $option) {
    if ($option) {
      list($entity_type, $bundle) = explode('--', $option);
      $selections[$entity_type][] = $bundle;
    }
  }
  variable_set('ckeditor_entity_embed_entity_bundles', $selections);

  // Save configurable plugin labels.
  variable_set('ckeditor_entity_embed_plugin_label_plugin', $form_state['values']['label-plugin']);
  variable_set('ckeditor_entity_embed_plugin_label_search', $form_state['values']['label-search']);
}

/**
 * Provides default values for the entity-bundles element within configuration.
 *
 * @return array
 *   A one-dimensional (necessary for the Form API) array with previously
 *   selected entity types and bundles. An example key and value pair would be
 *   "node--article" => "node--article". A combination that was not selected
 *   will not be represented in this array.
 */
function ckeditor_entity_embed_admin_entity_bundle_default_values() {
  $selected = variable_get('ckeditor_entity_embed_entity_bundles', array());
  $default_values = array();

  // Re-format the data stored in ckeditor_entity_embed_admin_submit() because
  // two-dimensional arrays cannot be used for default values.
  foreach ($selected as $entity_type => $bundles) {
    foreach ($bundles as $bundle) {
      $value = sprintf('%s--%s', $entity_type, $bundle);
      $default_values[$value] = $value;
    }
  }

  return $default_values;
}

/**
 * Provides options for the entity-bundles element within configuration.
 *
 * @return array
 *   A one dimensional array (necessary for the Form API) with all entity types
 *   and bundles that have view callbacks defined in hook_entity_info(). An
 *   example value would be "node--article".
 */
function ckeditor_entity_embed_admin_entity_bundle_options() {
  $entity_types = entity_get_info();
  $bundle_options = array();

  foreach ($entity_types as $entity_type => $entity_type_info) {
    if (!empty($entity_type_info['view callback']) && !empty($entity_type_info['bundles'])) {
      foreach ($entity_type_info['bundles'] as $bundle => $bundle_info) {
        $key = sprintf('%s--%s', $entity_type, $bundle);
        $value = sprintf('%s (%s)', $bundle_info['label'], $entity_type_info['label']);
        $bundle_options[$key] = $value;
      }
    }
  }
  ksort($bundle_options);

  return $bundle_options;
}
