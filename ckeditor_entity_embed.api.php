<?php

/**
 * @file
 * Hooks provided by the CKEditor Entity Embed module.
 */

/**
 * Alter the autocomplete results before they're provided to the plugin.
 *
 * When a user searches for content to embed, this is called after the results
 * are compiled but before they are returned to the user.
 *
 * @param array $results
 *   The results for the given string searched by the user.
 */
function hook_ckeditor_entity_embed_autocomplete_alter(&$results) {
  // Don't include node/1 in the possible results.
  foreach ($results as $key => $result) {
    if (strpos($result, 'node:1')) {
      unset($results[$key]);
    }
  }
}
